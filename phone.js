function phoneNrsAreEqual(nr1, nr2)
{
    nr1 = nr1.replace(' ', '');
    nr2 = nr2.replace(' ', '');
    if (nr1.startsWith('+421'))
    {
        nr1 = nr1.substr(4);
    }
    if (nr2.startsWith('+421'))
    {
        nr2 = nr2.substr(4);
    }
    if (nr1.startsWith('00421'))
    {
        nr1 = nr1.substr(5);
    }
    if (nr2.startsWith('00421'))
    {
        nr2 = nr2.substr(5);
    }
    if (nr1.length > 9 && nr1.startsWith('421'))
    {
        nr1 = nr1.substr(3);
    }
    if (nr2.length > 9 && nr2.startsWith('421'))
    {
        nr2 = nr2.substr(3);
    }
    if (nr1.startsWith('+'))
    {
        nr1 = nr1.substr(1);
    }
    if (nr2.startsWith('+'))
    {
        nr2 = nr2.substr(1);
    }
    if (nr1.startsWith('00'))
    {
        nr1 = nr1.substr(2);
    }
    if (nr2.startsWith('00'))
    {
        nr2 = nr2.substr(2);
    }
    var i;
    for (i = 0; i < 3; i++)
    {
        nr1 = nr1.replace('/', '');
        nr1 = nr1.replace('-', '');
        nr1 = nr1.replace('(', '');
        nr1 = nr1.replace(')', '');
        nr1 = nr1.replace(' ', '');
        nr2 = nr2.replace('/', '');
        nr2 = nr2.replace('-', '');
        nr2 = nr2.replace('(', '');
        nr2 = nr2.replace(')', '');
        nr2 = nr2.replace(' ', '');
    }
    if (nr1.startsWith('0'))
    {
        nr1 = nr1.substr(1);
    }
    if (nr2.startsWith('0'))
    {
        nr2 = nr2.substr(1);
    }
    if (nr1.length > 9 && nr1[2] == 0)
    {
        nr1 = nr1.substring(0, 2) + nr1.substr(3);
    }
    if (nr2.length > 9 && nr2[2] == 0)
    {
        nr2 = nr2.substring(0, 2) + nr2.substr(3);
    }
    if (nr1 == nr2)
    {
        return true;
    }
    else
    {
        return false;
    }
}
module.exports = phoneNrsAreEqual;
const phoneNrsAreEqual = require('./phone');
test('mobil', () =>
{
    expect(phoneNrsAreEqual('0', '0')).toBeTruthy();
    var number = [];
    var start = [];
    var phone = [];
    var i, j;
    start[0] = '+421';
    start[1] = '+421 ';
    start[2] = '+421-';
    start[3] = '+421/';
    start[4] = '421';
    start[5] = '421 ';
    start[6] = '421-';
    start[7] = '421/';
    start[8] = '00421';
    start[9] = '00421 ';
    start[10] = '00421-';
    start[11] = '00421/';
    start[12] = '00 421';
    start[13] = '00 421 ';
    start[14] = '00 421-';
    start[15] = '00 421/';
    start[16] = '0';
    number[0] = '905 456 789';
    number[1] = '905 456-789';
    number[2] = '905 456/789';
    number[3] = '905 456789';
    number[4] = '905-456 789';
    number[5] = '905-456-789';
    number[6] = '905-456/789';
    number[7] = '905-456789';
    number[8] = '905/456 789';
    number[9] = '905/456-789';
    number[10] = '905/456/789';
    number[11] = '905/456789';
    number[12] = '905456 789';
    number[13] = '905456-789';
    number[14] = '905456/789';
    number[15] = '905456789';
    for (i = 0; i < 17; i++)
    {
        for (j = 0; j < 16; j++)
        {
            phone[16 * i + j] = start[i] + number[j];
        }
    }
    for (i = 0; i < 272; i++)
    {
        for (j = 0; j < 272; j++)
        {
            expect(phoneNrsAreEqual(phone[i], phone[j])).toBeTruthy();
        }
    }
});
test('telefon', () =>
{
    var country = [];
    var city = [];
    var start = [];
    var number = [];
    var phone = [];
    var i, j;
    country[0] = '+421';
    country[1] = '421';
    country[2] = '00421';
    country[3] = '00 421';
    country[4] = '';
    city[0] = '51';
    city[1] = '051';
    city[2] = ' 51';
    city[3] = ' 051';
    city[4] = '51 ';
    city[5] = '051 ';
    city[6] = ' 51 ';
    city[7] = ' 051 ';
    city[8] = '51-';
    city[9] = '051-';
    city[10] = ' 51-';
    city[11] = ' 051-';
    city[12] = '51/';
    city[13] = '051/';
    city[14] = ' 51/';
    city[15] = ' 051/';
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 16; j++)
        {
            start[16 * i + j] = country[i] + city[j];
        }
    }
    number[0] = '765 4321';
    number[1] = '765-4321';
    number[2] = '765/4321';
    number[3] = '7654321';
    for (i = 0; i < 80; i++)
    {
        for (j = 0; j < 4; j++)
        {
            phone[4 * i + j] = start[i] + number[j];
        }
    }
    for (i = 0; i < 320; i++)
    {
        for (j = 0; j < 320; j++)
        {
            expect(phoneNrsAreEqual(phone[i], phone[j])).toBeTruthy();
        }
    }
});
test('nemecko', () =>
{
    var country = [];
    var city = [];
    var start = [];
    var number = [];
    var phone = [];
    country[0] = '+49';
    country[1] = '49';
    country[2] = '0049';
    country[3] = '00 49';
    city[0] = '30';
    city[1] = '030';
    city[2] = ' 30';
    city[3] = ' 030';
    city[4] = '30 ';
    city[5] = '030 ';
    city[6] = ' 30 ';
    city[7] = ' 030 ';
    city[8] = '30-';
    city[9] = '030-';
    city[10] = ' 30-';
    city[11] = ' 030-';
    city[12] = '30/';
    city[13] = '030/';
    city[14] = ' 30/';
    city[15] = ' 030/';
    city[16] = '(30)';
    city[17] = '(030)';
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 18; j++)
        {
            start[18 * i + j] = country[i] + city[j];
        }
    }
    number[0] = '1234-5678';
    number[1] = '1234/5678';
    number[2] = '1234 5678';
    number[3] = '12345678';
    for (i = 0; i < 72; i++)
    {
        for (j = 0; j < 4; j++)
        {
            phone[4 * i + j] = start[i] + number[j];
        }
    }
    for (i = 0; i < 288; i++)
    {
        for (j = 0; j < 288; j++)
        {
            expect(phoneNrsAreEqual(phone[i], phone[j])).toBeTruthy();
        }
    }
});
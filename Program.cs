﻿using System;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTest
{
    class Program
    {
        private static void Test1SK()
        {
            RemoteWebDriver driver = new EdgeDriver();
            driver.Navigate().GoToUrl("https://www.bart.sk/mam-zaujem-test");
            File.AppendAllText("report.log", "Test 1 SK\n"); 
            IWebElement element;
            try
            {
                element = driver.FindElement(By.Name("name"));
            }
            catch(NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole name.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("company"));
            }
            catch(NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole company.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("email"));
            }
            catch(NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole email.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("tel"));
            }
            catch(NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole tel.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("interest[dizajn]"));
            }
            catch(NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole dizajn.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("interest[programovanie]"));
            }
            catch(NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole programovanie.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("interest[online-marketing]"));
            }
            catch(NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole online-marketing.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("interest[webova-mobilna]"));
            }
            catch(NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole webova-mobilna.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("interest[elektronicky-obchod]"));
            }
            catch(NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole elektronicky-obchod.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("interest[webova-prezentacia]"));
            }
            catch(NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole webova-prezentacia.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("interest[cms]"));
            }
            catch(NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole cms.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("message"));
            }
            catch(NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole message.\n");
            }
            try
            {
                element = driver.FindElement(By.Id("contact-submit"));
            }
            catch(NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené tlačidlo contact.\n");
            }
            driver.Quit();
        }

        private static void Test1EN()
        {
            RemoteWebDriver driver = new EdgeDriver();
            driver.Navigate().GoToUrl("https://www.bart.sk/en/interested-in-test");
            File.AppendAllText("report.log", "Test 1 EN\n");
            IWebElement element;
            try
            {
                element = driver.FindElement(By.Name("name"));
            }
            catch (NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole name.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("company"));
            }
            catch (NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole company.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("email"));
            }
            catch (NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole email.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("tel"));
            }
            catch (NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole tel.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("interest[dizajn]"));
            }
            catch (NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole dizajn.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("interest[programovanie]"));
            }
            catch (NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole programovanie.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("interest[online-marketing]"));
            }
            catch (NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole online-marketing.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("interest[webova-mobilna]"));
            }
            catch (NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole webova-mobilna.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("interest[elektronicky-obchod]"));
            }
            catch (NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole elektronicky-obchod.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("interest[webova-prezentacia]"));
            }
            catch (NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole webova-prezentacia.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("interest[cms]"));
            }
            catch (NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole cms.\n");
            }
            try
            {
                element = driver.FindElement(By.Name("message"));
            }
            catch (NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené pole message.\n");
            }
            try
            {
                element = driver.FindElement(By.Id("contact-submit"));
            }
            catch (NoSuchElementException)
            {
                File.AppendAllText("report.log", "Nenájdené tlačidlo contact.\n");
            }
            driver.Quit();
        }

        private static void Test2SK()
        {
            RemoteWebDriver driver = new EdgeDriver();
            driver.Navigate().GoToUrl("https://www.bart.sk/mam-zaujem-test");
            File.AppendAllText("report.log", "Test 2 SK\n");
            try
            {
                IWebElement element;
                element = driver.FindElement(By.Name("name"));
                element.SendKeys("meno");
                element = driver.FindElement(By.Name("company"));
                element.SendKeys("firma");
                element = driver.FindElement(By.Name("email"));
                element.SendKeys("a@abc.sk");
                element = driver.FindElement(By.Name("tel"));
                element.SendKeys("0905123456");
                element = driver.FindElement(By.Name("interest[dizajn]"));
                element.Click();
                element = driver.FindElement(By.Name("interest[programovanie]"));
                element.Click();
                element = driver.FindElement(By.Name("interest[online-marketing]"));
                element.Click();
                element = driver.FindElement(By.Name("interest[webova-mobilna]"));
                element.Click();
                element = driver.FindElement(By.Name("interest[elektronicky-obchod]"));
                element.Click();
                element = driver.FindElement(By.Name("interest[webova-prezentacia]"));
                element.Click();
                element = driver.FindElement(By.Name("interest[cms]"));
                element.Click();
                element = driver.FindElement(By.Name("message"));
                element.SendKeys("popis");
                element = driver.FindElement(By.Id("contact-submit"));
                element.Click();
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
                IWebElement result = wait.Until(drv =>
                {
                    try
                    {
                        return drv.FindElement(By.ClassName("modal-box")).FindElement(By.TagName("img"));
                    }
                    catch (NoSuchElementException)
                    {
                        return null;
                    }
                });
                if (result == null)
                {
                    File.AppendAllText("report.log", "Neprišla žiadna odpoveď\n");
                }
            }
            catch(Exception exception) when 
            (exception is ElementClickInterceptedException || exception is UnhandledAlertException) 
            {
                File.AppendAllText("report.log", "Nedá sa používať aplikácia.\n");
            }
            driver.Quit();
        }

        private static void Test2EN()
        {
            RemoteWebDriver driver = new EdgeDriver();
            driver.Navigate().GoToUrl("https://www.bart.sk/en/interested-in-test");
            File.AppendAllText("report.log", "Test 2 EN\n");
            try
            {
                IWebElement element;
                element = driver.FindElement(By.Name("name"));
                element.SendKeys("meno");
                element = driver.FindElement(By.Name("company"));
                element.SendKeys("firma");
                element = driver.FindElement(By.Name("email"));
                element.SendKeys("a@abc.sk");
                element = driver.FindElement(By.Name("tel"));
                element.SendKeys("0905123456");
                element = driver.FindElement(By.Name("interest[dizajn]"));
                element.Click();
                element = driver.FindElement(By.Name("interest[programovanie]"));
                element.Click();
                element = driver.FindElement(By.Name("interest[online-marketing]"));
                element.Click();
                element = driver.FindElement(By.Name("interest[webova-mobilna]"));
                element.Click();
                element = driver.FindElement(By.Name("interest[elektronicky-obchod]"));
                element.Click();
                element = driver.FindElement(By.Name("interest[webova-prezentacia]"));
                element.Click();
                element = driver.FindElement(By.Name("interest[cms]"));
                element.Click();
                element = driver.FindElement(By.Name("message"));
                element.SendKeys("popis");
                element = driver.FindElement(By.Id("contact-submit"));
                element.Click();
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
                IWebElement result = wait.Until(drv =>
                {
                    try
                    {
                        return drv.FindElement(By.ClassName("modal-box")).FindElement(By.TagName("img"));
                    }
                    catch (NoSuchElementException)
                    {
                        return null;
                    }
                });
                if (result == null)
                {
                    File.AppendAllText("report.log", "Neprišla žiadna odpoveď\n");
                }
            }
            catch (Exception exception) when
            (exception is ElementClickInterceptedException || exception is UnhandledAlertException)
            {
                File.AppendAllText("report.log", "Neprišla odpoveď o odoslaní");
            }
            driver.Quit();
        }

        private static void Test3SK()
        {
            RemoteWebDriver driver = new EdgeDriver();
            driver.Navigate().GoToUrl("https://www.bart.sk/mam-zaujem-test");
            File.AppendAllText("report.log", "Test 3 SK\n");
            try
            {
                IWebElement elementName = driver.FindElement(By.Name("name"));
                IWebElement elementCompany = driver.FindElement(By.Name("company"));
                elementCompany.SendKeys("firma");
                IWebElement elementMail = driver.FindElement(By.Name("email"));
                elementMail.SendKeys("a@abc.sk");
                IWebElement elementTel = driver.FindElement(By.Name("tel"));
                elementTel.SendKeys("0905123456");
                IWebElement element = driver.FindElement(By.Name("interest[dizajn]"));
                element.Click();
                IWebElement elementMessage = driver.FindElement(By.Name("message"));
                elementMessage.SendKeys("popis");
                IWebElement elementSubmit = driver.FindElement(By.Id("contact-submit"));
                elementSubmit.Click();
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole name\n");
                }
                elementName.SendKeys("meno");
                elementCompany.Clear();
                elementSubmit.Click();
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole company\n");
                }
                elementCompany.SendKeys("firma");
                elementMail.Clear();
                elementSubmit.Click();
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole email\n");
                }
                elementMail.SendKeys("aaa");
                elementSubmit.Click();
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole email - aaa\n");
                }
                elementMail.Clear();
                elementMail.SendKeys("a@a");
                elementSubmit.Click();
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole email - a@a\n");
                }
                elementMail.Clear();
                elementMail.SendKeys("abc.sk");
                elementSubmit.Click();
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole email - abc.sk\n");
                }
                elementMail.Clear();
                elementTel.Clear();
                elementMail.SendKeys("a@abc.sk");
                elementSubmit.Click();
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole tel\n");
                }
                elementTel.SendKeys("abcdef");
                elementSubmit.Click();
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole tel - abcdef\n");
                }
                elementTel.Clear();
                elementTel.SendKeys("a1b2");
                elementSubmit.Click();
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole tel - a1b2\n");
                }
                elementTel.Clear();
                elementTel.SendKeys("0905123456");
                elementMessage.Clear();
                elementSubmit.Click();
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole message\n");
                }
                element = driver.FindElement(By.Name("interest[dizajn]"));
                element.Click();
                elementMessage.SendKeys("popis");
                elementSubmit.Click();
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole interest\n");
                }
            }
            catch (Exception exception) when
            (exception is ElementClickInterceptedException || exception is UnhandledAlertException)
            {
                File.AppendAllText("report.log", "Nedá sa používať aplikácia.\n");
            }
            driver.Quit();
        }

        private static void Test3EN()
        {
            RemoteWebDriver driver = new EdgeDriver();
            driver.Navigate().GoToUrl("https://www.bart.sk/en/interested-in-test");
            File.AppendAllText("report.log", "Test 3 EN\n");
            try
            {
                IWebElement elementName = driver.FindElement(By.Name("name"));
                IWebElement elementCompany = driver.FindElement(By.Name("company"));
                elementCompany.SendKeys("firma");
                IWebElement elementMail = driver.FindElement(By.Name("email"));
                elementMail.SendKeys("a@abc.sk");
                IWebElement elementTel = driver.FindElement(By.Name("tel"));
                elementTel.SendKeys("0905123456");
                IWebElement element = driver.FindElement(By.Name("interest[dizajn]"));
                element.Click();
                IWebElement elementMessage = driver.FindElement(By.Name("message"));
                elementMessage.SendKeys("popis");
                IWebElement elementSubmit = driver.FindElement(By.Id("contact-submit"));
                elementSubmit.Click();
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole name\n");
                }
                elementName.SendKeys("meno");
                elementCompany.Clear();
                elementSubmit.Click();
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole company\n");
                }
                elementCompany.SendKeys("firma");
                elementMail.Clear();
                elementSubmit.Click();
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole email\n");
                }
                elementMail.SendKeys("mail");
                elementSubmit.Click();
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole email\n");
                }
                elementMail.Clear();
                elementMail.SendKeys("a@a");
                elementSubmit.Click();
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole email - a@a\n");
                }
                elementMail.Clear();
                elementMail.SendKeys("abc.sk");
                elementSubmit.Click();
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole email - abc.sk\n");
                }
                elementMail.Clear();
                elementTel.Clear();
                elementMail.SendKeys("a@abc.sk");
                elementSubmit.Click();
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole tel\n");
                }
                elementTel.SendKeys("abcdef");
                elementSubmit.Click();
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole tel - abcdef\n");
                }
                elementTel.Clear();
                elementTel.SendKeys("a1b2");
                elementSubmit.Click();
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole tel - a1b2\n");
                }
                elementTel.SendKeys("0905123456");
                elementMessage.Clear();
                elementSubmit.Click();
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole message\n");
                }
                element = driver.FindElement(By.Name("interest[dizajn]"));
                element.Click();
                elementMessage.SendKeys("popis");
                elementSubmit.Click();
                try
                {
                    element = wait.Until(drv =>
                    {
                        try
                        {
                            return drv.FindElement(By.ClassName("error-text"));
                        }
                        catch (NoSuchElementException)
                        {
                            return null;
                        }
                    });
                }
                catch (WebDriverTimeoutException)
                {
                    element = null;
                }
                if (element == null)
                {
                    File.AppendAllText("report.log", "Nie je žiadna chybová správa - pole interest\n");
                }
            }
            catch (Exception exception) when
            (exception is ElementClickInterceptedException || exception is UnhandledAlertException)
            {
                File.AppendAllText("report.log", "Nedá sa používať aplikácia.\n");
            }
            driver.Quit();
        }

        static void Main(string[] args)
        {
            Test1SK();
            Test1EN();
            Test2SK();
            Test2EN();
            Test3SK();
            Test3EN();
        }
    }
}
